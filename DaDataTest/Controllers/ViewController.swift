//
//  ViewController.swift
//  DaDataTest
//
//  Created by Михаил on 15.12.2020.
//

import UIKit

class ViewController: UITableViewController {

    //MARK: - Vars
    private let searchController = UISearchController(searchResultsController: nil)
    private var timer: Timer?
    private var suggestions = [AddressSuggestions]() {
        didSet { tableView.reloadData() }
    }
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }

    //MARK: - API
    private func getAdress(adress: String) {
        NetworkService().fetchAdress(searchText: adress) { (result) in
            guard let suggestions = result?.suggestions else { return }
            self.suggestions = suggestions
        }
    }

    //MARK: - SetupUI
    private func setupUI() {
        tableView.tableFooterView = UIView()
        tableView.alwaysBounceVertical = true
        tableView.keyboardDismissMode = .interactive
        navigationItem.title = "Search adress"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search adress"
        searchController.searchResultsUpdater = self
        definesPresentationContext = true
    }
    
}

//MARK: - UITableView dataSource
extension ViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggestions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AddressCell
        let model = AddressViewModel(address: suggestions[indexPath.row])
        cell.configure(model)
        return cell
    }
}

//MARK: - UITableView Delegate
extension ViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = AddressViewModel(address: suggestions[indexPath.row])
        if model.coordinateExist {
            let mapView = MapViewController()
            mapView.model = model
            navigationController?.pushViewController(mapView, animated: true)
        }
    }
}

//MARK: - UISearchResultsUpdating
extension ViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        if text.count > 3 {
            timer?.invalidate()
            timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
                self.getAdress(adress: text)
            })
        } else {
            suggestions = []
        }
    }
}

//MARK: - UISearchBarDelegate
extension ViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        suggestions = []
    }
}
