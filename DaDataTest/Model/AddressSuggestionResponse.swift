//
//  AddressSuggestionResponse.swift
//  DaDataTest
//
//  Created by Михаил on 15.12.2020.
//

import Foundation

public struct AddressSuggestionResponse : Decodable {
    public let suggestions : [AddressSuggestions]?
}

public struct AddressSuggestions : Decodable {

    public let value : String?
    public let data : AddressSuggestionData?
    public let unrestrictedValue : String?
    
    enum CodingKeys: String, CodingKey {
        case value
        case data
        case unrestrictedValue = "unrestricted_value"
    }
}

public struct AddressSuggestionData : Decodable {
    public let geoLat : String?
    public let geoLon : String?
    
    enum CodingKeys: String, CodingKey {
        case geoLat = "geo_lat"
        case geoLon = "geo_lon"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        geoLat = try values.decodeIfPresent(String.self, forKey: .geoLat)
        geoLon = try values.decodeIfPresent(String.self, forKey: .geoLon)
    }
}
