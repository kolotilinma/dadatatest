//
//  AddressViewModel.swift
//  DaDataTest
//
//  Created by Михаил on 16.12.2020.
//

import UIKit
import CoreLocation

struct AddressViewModel {
    
    private var address: AddressSuggestions
    
    var adressText: String? {
        return address.unrestrictedValue
    }
    
    var shortAddressText: String? {
        let text = "\(address.value ?? "")"
        return text
    }
    
    var coordinateExist: Bool  {
        guard let addressData = address.data else { return false }
        guard addressData.geoLat != nil else { return false }
        guard addressData.geoLon != nil else { return false }
        return true
    }
    
    private var latitude: Double {
        let latString = address.data?.geoLat ?? ""
        guard let lat = Double(latString) else { return 0 }
        return lat
    }
    
    private var longitude: Double {
        let lonString = address.data?.geoLon ?? ""
        guard let lon = Double(lonString) else { return 0 }
        return lon
    }
    
    var location: CLLocation {
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    var mapTintColor: UIColor { return coordinateExist ? #colorLiteral(red: 1, green: 0.4745098039, blue: 0.4156862745, alpha: 1) : .black }
    var mapImage: UIImage {
        return coordinateExist ? UIImage(systemName: "mappin.and.ellipse")! : UIImage(systemName: "mappin.slash")!
    }
    
    init(address: AddressSuggestions) {
        self.address = address
    }
    
}
