//
//  AddressCell.swift
//  DaDataTest
//
//  Created by Михаил on 16.12.2020.
//

import UIKit

class AddressCell: UITableViewCell {
    
    
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var mapImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func configure(_ adressModel: AddressViewModel) {
        self.adressLabel.text = adressModel.adressText
        self.mapImage.image = adressModel.mapImage
        self.mapImage.tintColor = adressModel.mapTintColor
        self.accessoryType = adressModel.coordinateExist ? .disclosureIndicator : .none
        self.selectionStyle = adressModel.coordinateExist ? .default : .none
    }
    
    
}
