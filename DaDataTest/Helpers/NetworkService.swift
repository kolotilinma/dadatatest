//
//  NetworkService.swift
//  DaDataTest
//
//  Created by Михаил on 16.12.2020.
//

import UIKit
import Alamofire

class NetworkService {
    
    func fetchAdress(searchText: String, completion: @escaping (AddressSuggestionResponse?) -> Void) {
        let url = Constants.API_URL
        let parameters = ["token":"\(Constants.API_KEY)",
                          "query":"\(searchText)",
                          "count":"15"]
        
        AF.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseData { (dataResponse) in
            if let error = dataResponse.error {
                print("DEBUG: received requestiong data: \(error.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let data = dataResponse.data else { return }
            let decoder = JSONDecoder()
            do {
                let objects = try decoder.decode(AddressSuggestionResponse.self, from: data)
                completion(objects)
            } catch let jsonError {
                print("DEBUG: Failed to decode JSON", jsonError.localizedDescription)
                completion(nil)
            }
        }
    }
    
}

